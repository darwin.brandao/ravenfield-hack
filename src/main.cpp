#include "X11/keysym.h"
#include "cheat-table/ce_table.cpp"
#include "proc/proc.h"
#include <X11/Xlib.h>
#include <iostream>
#include <thread>
#include <vector>

// https://stackoverflow.com/questions/24708700/c-detect-when-user-presses-arrow-key
// #define KEY_UP 72      // Up arrow character
// #define KEY_DOWN 80    // Down arrow character
// #define KEY_ENTER '\n' // Enter key charatcer

bool key_is_pressed(KeySym ks) {
  Display *dpy = XOpenDisplay(":0");
  char keys_return[32];
  XQueryKeymap(dpy, keys_return);
  KeyCode kc2 = XKeysymToKeycode(dpy, ks);
  bool isPressed = !!(keys_return[kc2 >> 3] & (1 << (kc2 & 7)));
  XCloseDisplay(dpy);
  return isPressed;
}

void handleInput(pid_t procId, ProcessManager procMan,
                 std::vector<EntryValue> entryValues) {

  for (unsigned int i = 0; i < entryValues.size(); i++) {

    if (key_is_pressed(XK_KP_4)) {
      if (entryValues[i].CheatEngineEntry.Description == "\"SwimSpeed\"") {
        procMan.writeMemory<float>(procId, (void *)entryValues[i].ValueAddr,
                                   60.0f);
      }
    }

    if (key_is_pressed(XK_KP_5)) {
      if (entryValues[i].CheatEngineEntry.Description == "\"JumpVelocity\"") {
        procMan.writeMemory<float>(procId, (void *)entryValues[i].ValueAddr,
                                   10.0f);
      }
    }

    if (key_is_pressed(XK_KP_6)) {
      if (entryValues[i].CheatEngineEntry.Description == "\"RunSpeed\"") {
        procMan.writeMemory<float>(procId, (void *)entryValues[i].ValueAddr,
                                   13.0f);
      }
    }

    if (key_is_pressed(XK_KP_8)) {
      if (entryValues[i].CheatEngineEntry.Description == "\"Ammo\"") {
        procMan.writeMemory<int>(procId, (void *)entryValues[i].ValueAddr,
                                 9999);
      }
    }

    if (key_is_pressed(XK_KP_9)) {
      if (entryValues[i].CheatEngineEntry.Description == "\"Health\"") {
        procMan.writeMemory<float>(procId, (void *)entryValues[i].ValueAddr,
                                   9999.0f);
      }
    }

    if (entryValues[i].EntryValues.size() > 0) {
      handleInput(procId, procMan, entryValues[i].EntryValues);
    }
  }
}

int main(int argc, char *argv[]) {

  // Check if a process name was specified
  if (argc == 0 || argv[1] == NULL) {
    std::cout << "No process name specified. Please specify a process name as "
                 "an argument."
              << std::endl;
    return 1;
  }

  // Check if a Cheat Table was specified
  if (argv[2] == NULL) {
    std::cout << "No Cheat Table specified." << std::endl;
    return 1;
  }

  // Load cheat table
  rapidxml::xml_document<> doc;
  rapidxml::file<> xmlFile(argv[2]); //"./src/cheat-table2.CT"

  CheatTable ct = CheatTable();

  doc.parse<0>(xmlFile.data());

  rapidxml::xml_node<> *pRoot = doc.first_node();
  std::vector<CheatEntry> entries = ct.getCheatEntries(pRoot);

  // Process
  ProcessManager procMan;

  // Get Process Info
  ProcessInfo procInfo = procMan.getProcessInfo(argv[1]);

  // Check if process id was found
  if (procInfo.Id == -1) {
    std::cout << "Process could not be found" << std::endl;
    return 1;
  }

  // Get base address of the entry point
  ProcessModule pm = procMan.getProcessModule(entries[0].Address);
  long baseAddress = procMan.findBaseAddress(procInfo.Id, pm.Name, pm.Index);

  // Add entry point offset to base address
  baseAddress += pm.Offset;

  while (true) {

    // Clear screen on Linux and Unix-Like systems
    system("clear");

    std::cout << "Input Name: \t\t" << (std::string)argv[1] << std::endl
              << "Module Name: \t\t"
              << "[ 0x" << baseAddress << " ] " << pm.Name << std::endl
              << "Process Found: \t\t"
              << "[ " << procInfo.Id << " ] " << procInfo.Name << std::endl
              << "Cheat Table: \t\t" << argv[2] << std::endl
              << std::endl;

    // Parse cheat entries from cheat table and print to the screen
    std::vector<EntryValue> entryValues =
        ct.getEntriesValues(procInfo.Id, procMan, entries, baseAddress);

    handleInput(procInfo.Id, procMan, entryValues);

    std::cout << "Keys Pressed:" << std::endl;
    std::cout << "[KP_4] SwimSpeed \t"
              << (key_is_pressed(XK_KP_4) ? "True" : "False") << std::endl;
    std::cout << "[KP_5] JumpVelocity \t"
              << (key_is_pressed(XK_KP_5) ? "True" : "False") << std::endl;
    std::cout << "[KP_6] RunSpeed \t"
              << (key_is_pressed(XK_KP_6) ? "True" : "False") << std::endl;
    std::cout << "[KP_8] Ammo \t\t"
              << (key_is_pressed(XK_KP_8) ? "True" : "False") << std::endl;
    std::cout << "[KP_9] Health \t\t"
              << (key_is_pressed(XK_KP_9) ? "True" : "False") << std::endl;

    // Sleep
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }
}