#include "../../lib/rapidxml-1.13/rapidxml.hpp"
#include "../../lib/rapidxml-1.13/rapidxml_utils.hpp"
#include "../proc/proc.h"
#include <algorithm>
#include <vector>

struct CheatEntry {
  int ID;
  std::string Description;
  bool ShowSigned;
  std::string VariableType;
  std::string Address;
  std::vector<unsigned int> Offsets;
  std::vector<CheatEntry> CheatEntries;
};

struct EntryValue {
  CheatEntry CheatEngineEntry;
  uintptr_t ValueAddr;
  size_t ValueSize;
  std::vector<EntryValue> EntryValues;
};

struct CheatTable {

  CheatEntry getCheatEntry(rapidxml::xml_node<> *pNode) {
    CheatEntry ce = CheatEntry();

    // Loop through all subnodes
    for (rapidxml::xml_node<> *pAttribute = pNode->first_node(); pAttribute;
         pAttribute = pAttribute->next_sibling()) {

      // Get tag name and value
      std::string name = pAttribute->name();
      std::string value = pAttribute->value();

      // Assign XML tags to CheatEngine object attributes
      if (name == "ID")
        ce.ID = stoi(value);

      if (name == "Description")
        ce.Description = value;

      if (name == "ShowAsSigned")
        ce.ShowSigned = value == "1";

      if (name == "VariableType")
        ce.VariableType = value;

      if (name == "Address")
        ce.Address = value;

      if (name == "Offsets") {
        // Add offsets to list
        std::vector<unsigned int> offsets = std::vector<unsigned int>();

        for (rapidxml::xml_node<> *pOffset = pAttribute->first_node(); pOffset;
             pOffset = pOffset->next_sibling()) {
          offsets.push_back(std::strtol(pOffset->value(), NULL, 16));
        }

        // Reverse offset list and assign list to objetct
        std::reverse(offsets.begin(), offsets.end());
        ce.Offsets = offsets;
      }

      if (name == "CheatEntries")
        ce.CheatEntries = getCheatEntries(pNode);
    }

    return ce;
  }

  std::vector<CheatEntry> getCheatEntries(rapidxml::xml_node<> *pRoot) {
    std::vector<CheatEntry> entries = std::vector<CheatEntry>();

    // Loop through all root nodes
    for (rapidxml::xml_node<> *pNode = pRoot->first_node(); pNode;
         pNode = pNode->next_sibling()) {

      // Loop through all subnodes
      for (rapidxml::xml_node<> *pChild = pNode->first_node(); pChild;
           pChild = pChild->next_sibling()) {

        // Check if tag is "CheatEntry"
        std::string nodeName = (std::string)pChild->name();

        if (nodeName == "CheatEntry") {
          // Parse data from XML to object of type CheatEntry
          entries.push_back(getCheatEntry(pChild));
        }
      }
    }

    return entries;
  }

  std::vector<EntryValue> getEntriesValues(pid_t procId, ProcessManager procMan,
                                           std::vector<CheatEntry> entries,
                                           long parentAddress,
                                           unsigned int index = 0) {

    std::vector<EntryValue> output = std::vector<EntryValue>();

    // Return if entries vector is empty
    if (entries.size() == 0) {
      return output;
    }

    for (unsigned int i = 0; i < entries.size(); i++) {

      // Get offset from "Address" for nested entries
      unsigned int offset = 0;

      if (entries[i].Address[0] == '+') {
        // Remove "+" from string and convert to hexadecimal
        offset = stoul(
            entries[i].Address.substr(1, entries[i].Address.length()), 0, 16);
      }

      // Find dynamic address for the target memory address
      uintptr_t targetAddress = procMan.findDynamicMemoryAllocatedAddress(
          procId, parentAddress + offset, entries[i].Offsets);

      EntryValue entryValue = EntryValue();

      entryValue.CheatEngineEntry = entries[i];
      entryValue.ValueAddr = targetAddress;

      // Lower case for VariableType attribute from CheatEntry
      std::transform(entries[i].VariableType.begin(),
                     entries[i].VariableType.end(),
                     entries[i].VariableType.begin(), ::tolower);

      // Add tabs for hierarchy view
      //   for (unsigned int j = 0; j < index; j++) {
      //     std::cout << "\t";
      //   }

      // Print target address
      // std::cout << "[" << std::hex << targetAddress << "]" << std::dec;

      // Read memory for defined variable types
      if (entries[i].VariableType == "float") {
        entryValue.ValueSize = sizeof(float);
      } else if (entries[i].VariableType == "4 bytes") {
        entryValue.ValueSize = sizeof(int);
      } else if (entries[i].VariableType == "byte") {
        entryValue.ValueSize = sizeof(bool);
      }

      // Parse subentries in CheatEntries attriute from CheatEntry
      if (entries[i].CheatEntries.size() > 0) {
        entryValue.EntryValues =
            getEntriesValues(procId, procMan, entries[i].CheatEntries,
                             (long)targetAddress, index + 1);
      }

      output.push_back(entryValue);
    }

    return output;
  }
};