#pragma once

#include <cstdint>   // uintptr_t
#include <dirent.h>  // dir
#include <fstream>   // file
#include <string.h>  // string
#include <sys/uio.h> // process_vm_readv
#include <vector>    // lists

struct ProcessInfo {
  pid_t Id;
  std::string Name;
};

struct ProcessModule {
  std::string Name;
  unsigned int Index;
  unsigned int Offset;
};

class ProcessManager {

public:
  ProcessInfo getProcessInfo(std::string processName);

  ProcessModule getProcessModule(std::string input);

  uintptr_t findBaseAddress(pid_t processId, std::string module);
  uintptr_t findBaseAddress(pid_t processId, std::string module,
                            unsigned int index);

  uintptr_t
  findDynamicMemoryAllocatedAddress(pid_t procId, uintptr_t basePtr,
                                    std::vector<unsigned int> offsets);

  void readMemory(pid_t pid, void *source, void *destination, size_t size);
  void writeMemory(pid_t pid, void *destination, void *source, size_t size);

  template <typename T> T readMemory(pid_t procId, void *targetAddress) {
    T readBuffer;
    size_t size = sizeof(T);

    readMemory(procId, targetAddress, &readBuffer, size);
    return readBuffer;
  }

  template <typename T>
  T writeMemory(pid_t procId, void *targetAddress, T writeBuffer) {
    size_t size = sizeof(T);
    writeMemory(procId, targetAddress, &writeBuffer, size);
    return writeBuffer;
  }
};