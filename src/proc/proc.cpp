#include "proc.h"

ProcessInfo ProcessManager::getProcessInfo(std::string processName) {
  // Output pid starts as -1
  pid_t pid = (pid_t)-1;

  ProcessInfo pi = ProcessInfo();
  pi.Id = pid;

  // Check if processName is empty
  if (processName.empty())
    return pi;

  // Open /proc directory
  DIR *pdir = opendir("/proc");

  // Check if directory was open
  if (!pdir)
    return pi;

  // Pointer to dirent struct
  struct dirent *pdirent;

  // Read directory
  pdirent = readdir(pdir);

  // Check if it exists
  if (!pdirent)
    return pi;

  // Loop while there are entities in the directory and while pid is -1
  while (pdirent && pid == -1) {

    // Check if entity is a directory (d_type 4)
    if (pdirent->d_type == DT_DIR) {

      // Check if directory's name is a number
      if (atoi(pdirent->d_name) != 0) {

        // /proc/<pid>/status content
        std::fstream status;

        // /proc/<pid>/status path
        std::string statusPath =
            "/proc/" + (std::string)pdirent->d_name + "/status";

        // Open status file from /proc/<pid> on read mode
        status.open(statusPath, std::ios::in);

        // Check if file is open
        if (status.is_open()) {

          // Get first line
          std::string line;
          getline(status, line);

          // Check if first line contains the name of the given process
          if (line.find(processName) != std::string::npos) {
            pid = atoi(pdirent->d_name);

            pi.Id = pid;
            pi.Name = line.substr(6, line.length());
          }
        }

        // Close file handle
        status.close();
      }
    }

    // Read next entity
    pdirent = readdir(pdir);
  }

  // Close directory handle
  closedir(pdir);

  return pi;
}

ProcessModule ProcessManager::getProcessModule(std::string input) {

  ProcessModule pm = ProcessModule();

  // Get full process name between double quotes
  std::string tmp = input.substr(input.find("\"") + 1, input.find("\"", 1) - 1);

  // Get module name
  pm.Name = tmp.substr(0, tmp.find(".", tmp.find(".") + 1));

  // Get module index
  pm.Index =
      stoi(tmp.substr(tmp.find(".", tmp.find(".") + 1) + 1, tmp.length()));

  // Get offset from module
  pm.Offset = std::strtol(
      input.substr(input.find("+") + 1, input.length()).c_str(), NULL, 16);

  return pm;
}

uintptr_t ProcessManager::findBaseAddress(pid_t processId, std::string module) {
  return findBaseAddress(processId, module, 0);
}
uintptr_t ProcessManager::findBaseAddress(pid_t processId, std::string module,
                                          unsigned int index = 0) {

  uintptr_t baseAddress = -1;

  // Content from /proc/<pid>/maps
  std::fstream maps;

  // Open stream
  maps.open("/proc/" + std::to_string(processId) + "/maps", std::ios::in);

  // Current line from loop below
  std::string line;

  // Loop lines checking if stream is open and baseAddress is -1
  for (int i = 0; maps.is_open() && getline(maps, line) && baseAddress == -1;) {

    // Check if line contains module name
    if (line.find(module) != std::string::npos) {

      // Check for current index
      if (i < index) {
        i++;
        continue;
      }

      // Get position in line for the first '-' char
      long dashPos = line.find_first_of('-');

      // Get text (module address) from the first char to the '-' position
      std::string strAddress = line.substr(0, dashPos);

      // Convert module address from string to long
      baseAddress = std::strtol(strAddress.c_str(), nullptr, 16);
    }
  }

  // Close stream
  maps.close();

  return baseAddress;
}

uintptr_t ProcessManager::findDynamicMemoryAllocatedAddress(
    pid_t procId, uintptr_t basePtr, std::vector<unsigned int> offsets) {

  uintptr_t address = basePtr;

  // Loop offsets
  for (unsigned int i = 0; i < offsets.size(); i++) {

    // Read memory from address stored in "address" variable and write it to
    // address variable
    readMemory(procId, (uintptr_t *)address, &address, sizeof(address));

    // Add offset to address
    address += offsets[i];
  }

  return address;
}

void ProcessManager::readMemory(pid_t pid, void *source, void *destination,
                                size_t size) {

  /*
  pid  = target process id
  src  = address to read from on the target process
  dst  = address to write to on the caller process
  size = size of the buffer that will be read
  */

  struct iovec iosrc;
  struct iovec iodst;

  iodst.iov_base = destination;
  iodst.iov_len = size;

  iosrc.iov_base = source;
  iosrc.iov_len = size;

  process_vm_readv(pid, &iodst, 1, &iosrc, 1, 0);
}

void ProcessManager::writeMemory(pid_t pid, void *destination, void *source,
                                 size_t size) {

  /*
  pid  = target process id
  dst  = address to write to on the target process
  src  = address to read from on the caller process
  size = size of the buffer that will be read
  */

  struct iovec iosrc;
  struct iovec iodst;

  iosrc.iov_base = source;
  iosrc.iov_len = size;

  iodst.iov_base = destination;
  iodst.iov_len = size;

  process_vm_writev(pid, &iosrc, 1, &iodst, 1, 0);
}